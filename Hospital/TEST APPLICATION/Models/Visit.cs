﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TEST_APPLICATION.Models
{
    [Table("Visit")]
    [Serializable]
    public class Visit
    {
        public int Id { get; set; }
        [Required]
        public int PatientId { get; set; }
        [Required]
        public int DoctorId { get; set; }
        [Required]
        public int DiagnosisId { get; set; }
        [Required]
        public string Complaints { get; set; }
        [Required]
        public DateTime DateVisit { get; set; }
    }
}
