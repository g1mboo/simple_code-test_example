﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TEST_APPLICATION.Models
{
    [Table("Patient")]
    [Serializable]
    public class Patient
    {
        public int Id { get; set; }
        [Required]
        public string IdCardNumber { get; set; }
        [Required]
        public string FullName { get; set; }        
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
    }
}
