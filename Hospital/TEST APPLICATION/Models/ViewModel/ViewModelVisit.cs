﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TEST_APPLICATION.Models.ViewModel
{
    public class ViewModelVisit
    {
        public int Id { get; set; }        
        
        public string Doctor { get; set; }
        
        public string Diagnosis { get; set; }
        
        public string Complaints { get; set; }
        
        public DateTime DateVisit { get; set; }
    }
}
