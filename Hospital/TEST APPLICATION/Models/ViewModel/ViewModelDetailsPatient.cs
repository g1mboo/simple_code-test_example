﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TEST_APPLICATION.Models.ViewModel
{
    public class ViewModelDetailsPatient
    {
        public Patient Patient { get; set; }
        public IEnumerable<ViewModelVisit> Visits { get; set; }
    }
}
