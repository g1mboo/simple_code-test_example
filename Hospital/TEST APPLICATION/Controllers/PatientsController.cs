﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TEST_APPLICATION.Data;
using TEST_APPLICATION.Models;
using TEST_APPLICATION.Models.ViewModel;

namespace TEST_APPLICATION.Controllers
{
    public class PatientsController : Controller
    {
        private readonly HospitalContext _context;

        public PatientsController(HospitalContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Index(string request)
        {
            var patients = await _context.Patients.ToListAsync();

            if (!String.IsNullOrEmpty(request))
            {
                patients = patients.Where(s => s.FullName.Contains(request) || s.IdCardNumber.Contains(request)).ToList();
            }

            return View(patients);
        }

        [HttpGet]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patient = await _context.Patients
                .FirstOrDefaultAsync(m => m.Id == id);

            if (patient == null)
            {
                return NotFound();
            }   
            
            var visits = await _context.Visits.Where(item => item.PatientId == patient.Id).ToListAsync() as IEnumerable<Visit>;
            var viewModelVisits = new List<ViewModelVisit>();

            
            if (visits != null)
            {
                foreach (var item in visits)
                {
                    var diagnosis = _context.Diagnoses.Where(x => x.Id == item.DiagnosisId).FirstOrDefault();
                    var doctor = _context.Doctors.Where(x => x.Id == item.DoctorId).FirstOrDefault();

                    viewModelVisits.Add(new ViewModelVisit() 
                    { 
                        Id = item.Id,
                        Complaints = item.Complaints,
                        Doctor = doctor.FullName + '(' + doctor.Specialty + ')',
                        Diagnosis = diagnosis.Name,
                        DateVisit = item.DateVisit,

                    });
                }
            }

            var detailsViewModel = new ViewModelDetailsPatient()
            {
                Patient = patient,
                Visits = viewModelVisits
            };

            return View(detailsViewModel);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return PartialView();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdCardNumber,FullName,Address,PhoneNumber")] Patient patient)
        {
            if (ModelState.IsValid)
            {
                _context.Add(patient);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(patient);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {   
            if (id == null)
            {
                return NotFound();
            }

            var patient = await _context.Patients.FindAsync(id);
            if (patient == null)
            {
                return NotFound();
            }
            return PartialView(patient);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdCardNumber,FullName,Address,PhoneNumber")] Patient patient)
        {
            if (id != patient.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(patient);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PatientExists(patient.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(patient);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patient = await _context.Patients
                .FirstOrDefaultAsync(m => m.Id == id);
            if (patient == null)
            {
                return NotFound();
            }

            return PartialView(patient);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var patient = await _context.Patients.FindAsync(id);
            _context.Patients.Remove(patient);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PatientExists(int id)
        {
            return _context.Patients.Any(e => e.Id == id);
        }
    }
}
